import UIKit

//---------------------------------
// MARK: - Color Palette
// We can add colors in 2 formats: RGB or HEX
//---------------------------------

enum ColorPalette {
    static let clear = Color(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
    static let grey = Color(red: 0.56, green: 0.56, blue: 0.58, alpha: 1)
    static let lightGrey = Color(red: 0.78, green: 0.78, blue: 0.8, alpha: 1)
    static let blue = Color(red: 0.11, green: 0.72, blue: 0.82, alpha: 1)
    static let white = Color(stringLiteral: "FFFFFF")
    static let black = Color(stringLiteral: "000000")
    static let green = Color(stringLiteral: "92C03C")
    static let red = Color(stringLiteral: "F44336")
    static let darkRed = Color(stringLiteral: "C13A20")
}

class Color: UIColor {
    convenience public required init(stringLiteral value: String) {
        var hex = value.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if hex.hasPrefix("#") {
            hex.removeFirst()
        }
        if hex.count != 6 {
            fatalError("Unable to create color. Invalid code: \(value)")
        }

        var rgbValue: UInt32 = 0
        Scanner(string: hex).scanHexInt32(&rgbValue)

        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16 ) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8 ) / 255.0,
            blue: CGFloat((rgbValue & 0x0000FF) >> 0 ) / 255.0,
            alpha: 1.0
        )
    }
}
