import UIKit

public protocol ReusableView {
    static var reusableIdentifier: String { get }
}

// MARK: - UIView

extension ReusableView where Self: UIView {
    public static var reusableIdentifier: String {
        return String(describing: self)
    }
}
