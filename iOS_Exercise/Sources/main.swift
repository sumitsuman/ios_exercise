import UIKit

let isRunningUnitTests: Bool = NSClassFromString("XCTestCase") != nil
let applicationDelegateClassName: String? = isRunningUnitTests ? .none : NSStringFromClass(AppDelegate.self)
UIApplicationMain(CommandLine.argc, CommandLine.unsafeArgv, .none, applicationDelegateClassName)
