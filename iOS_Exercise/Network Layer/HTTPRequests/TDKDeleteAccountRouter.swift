

import Alamofire

enum MoviesResultRouter: URLRequestConvertible {
    
    case moviesResult(api_key: String, language: String,page: Int)
    
    
    
    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        switch self {
        case .moviesResult:
            return .get
        }
    }
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .moviesResult:
            return "popular"
        }
    }
    
    // MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
        case .moviesResult(let api_key, let lang, let page):
            return ["api_key": api_key, "language": lang, "page": page]
        }
    }

    var parameterEncoding: ParameterEncoding {
        switch self {
        case .moviesResult:
            return URLEncoding.queryString
        }
    }
    
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try K.ProductionServer.baseURL.asURL()
        let encoding = parameterEncoding
        var urlRequest = try encoding.encode(URLRequest(url: url.appendingPathComponent(path)), with: parameters)
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
		urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
//        let token = "Bearer "
//        urlRequest.setValue(token, forHTTPHeaderField: HTTPHeaderField.authentication.rawValue)
        // Parameters
        if let parameters = parameters {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions())
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        debugPrint("============Request==============",String(describing:urlRequest), String(describing:urlRequest.httpHeaders))
        return urlRequest
    }
    
}

class MoviesResultsRequest: NSObject {
    
    static func getMoviesResults(api_key: String, lang: String, pageNo: Int, completion:@escaping (Result<Any>)->Void) {
        let _ = APIClient.performJSONRequest(route: MoviesResultRouter.moviesResult(api_key: api_key, language: lang, page: pageNo ), completion: completion)
    }
}
