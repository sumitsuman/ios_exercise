
import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper


class APIClient: NSObject {
    @discardableResult
    private static func performRequest<T:Decodable>(route:URLRequestConvertible, decoder: JSONDecoder = JSONDecoder(), completion:@escaping (Result<T>)->Void) -> DataRequest {
        return AF.request(route)
            .responseDecodable (decoder: decoder){ (response: DataResponse<T>) in
                completion(response.result)
        }
    }
    
    static func performJSONRequest(route:URLRequestConvertible, completion:@escaping (Result<Any>)->Void) -> DataRequest {
        return AF.request(route)
            .responseJSON(completionHandler: { (response) in
                completion(response.result)
            })
    }
    
    static func performVideoRequest(movieId: Int, completion:@escaping (Result<Any>)->Void) -> DataRequest {
        return AF.request("https://api.themoviedb.org/3/movie/\(movieId)/videos", method: .get, parameters: ["api_key": "b1b4bf87e5648c80ba62a374ee4c16e4", "language": "en-US"])
            .validate(statusCode: 200..<300)
            .responseJSON {
                response in
                completion(response.result)
        }
    }
}
