import Foundation
import UIKit

final class PopularMoviesViewMOdel {
  
  enum Constants {
    static let pageSize: Int = 19
    static let zeroMovie: Int = 0
    static let offlineMessage: String = "INTERNET_WARNING".localized(bundle: .main)
    enum Grid {
      static let columns: Int = 2
      static let spacing: CGFloat = 1
      static let insets = UIEdgeInsets(top: 1, left: 1, bottom: 0, right: 1)
      static let cardSize = CGSize(width: 168, height: 168)
    }
  }
  
  private(set) var rootModel: RootViewModel?

  var insetsInSection: UIEdgeInsets {
    return Constants.Grid.insets
  }
  
  var itemsSpacing: CGFloat {
    return Constants.Grid.spacing
  }
  
  func sizeForItem(fromWidth width: CGFloat) -> CGSize {
    let columns = CGFloat(Constants.Grid.columns)
    let spacing: CGFloat = itemsSpacing * (columns - 1)
    let padding: CGFloat = insetsInSection.left + insetsInSection.right
    let adjustedWidth = (width - padding - spacing) / columns
    return Constants.Grid.cardSize.widthScaling(with: adjustedWidth)
  }
  
}
