import Foundation

extension Date {
    public func string(with dateFormatter: DateFormatter) -> String {
        return dateFormatter.string(from: self)
    }
}
