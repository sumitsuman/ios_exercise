import UIKit

class TrailerTableViewCell: UITableViewCell, NibNameableView, ReusableView {
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var siteLabel: UILabel!
	@IBOutlet weak var typeLable: UILabel!
	
	@IBOutlet weak var thumbNailImageView: UIImageView!
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
	
	func configure(with viewModel: TrailerViewModel) {
		nameLabel.text = viewModel.name
		siteLabel.text = "\(viewModel.site ) \(viewModel.size)"
		typeLable.text = viewModel.type
	}
    
}
