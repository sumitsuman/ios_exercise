//
//  SynopsisTableViewCell.swift
//  iOS_Exercise
//
//  Created by Laura Daniela on 5/12/19.
//  Copyright © 2019 iTexico. All rights reserved.
//

import UIKit

class SynopsisTableViewCell: UITableViewCell, NibNameableView, ReusableView {
	@IBOutlet weak var ratingLabel: UILabel!
	@IBOutlet weak var yearLabel: UILabel!
	@IBOutlet weak var runtimeLabel: UILabel!
	@IBOutlet weak var synopsisLabel: UILabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

	func configure(with viewModel: MovieCellViewModel?){
		if let rating = viewModel?.vote_average, let year = viewModel?.release_date, let synopis = viewModel?.overview{
			ratingLabel.text = "\(rating)/10"
			yearLabel.text = year
			synopsisLabel.text = synopis
			runtimeLabel.text = "_:_"
		}
	}
}
