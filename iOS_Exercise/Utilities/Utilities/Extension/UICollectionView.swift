import UIKit

extension UICollectionView {
    enum SupplementaryViewKind {
        case header
        case footer

        var rawValue: String {
            switch self {
            case .header:
                return UICollectionView.elementKindSectionHeader
            case .footer:
                return UICollectionView.elementKindSectionFooter
            }
        }
    }

    func register<Cell: UICollectionViewCell>(cell: Cell.Type, in bundle: Bundle?) where Cell: ReusableView, Cell: NibNameableView {
        let bundle = bundle ?? Bundle.main
        let nib = UINib(nibName: Cell.nibName, bundle: bundle)
        register(nib, forCellWithReuseIdentifier: Cell.reusableIdentifier)
    }

    func register<View: UICollectionReusableView>(view: View.Type, in bundle: Bundle, asSupplementaryViewKind kind: SupplementaryViewKind) where View: ReusableView, View: NibNameableView {
        let nib = UINib(nibName: View.nibName, bundle: bundle)
        register(nib, forSupplementaryViewOfKind: kind.rawValue, withReuseIdentifier: View.reusableIdentifier)
    }
}

extension UICollectionView {
    func dequeueReusableCell<Cell: UICollectionViewCell>(forIndexPath indexPath: IndexPath) -> Cell where Cell: ReusableView {
        guard let cell = dequeueReusableCell(withReuseIdentifier: Cell.reusableIdentifier, for: indexPath) as? Cell else {
            fatalError("Could not dequeue cell with identifier: \(Cell.reusableIdentifier)")
        }
        return cell
    }

    func dequeueReusableSupplementaryView<View: UICollectionReusableView>(ofKind kind: SupplementaryViewKind, forIndexPath indexPath: IndexPath) -> View where View: ReusableView {
        guard let view = dequeueReusableSupplementaryView(ofKind: kind.rawValue, withReuseIdentifier: View.reusableIdentifier, for: indexPath) as? View else {
            fatalError("Could not dequeue view with identifier: \(View.reusableIdentifier)")
        }
        return view
    }
}
