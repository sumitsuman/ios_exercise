import Foundation

 class RootViewModel {
	public var page: Int?
	public var total_results: Int?
	public var total_pages: Int?
	public var moviesCellViewModel: [MovieCellViewModel]? = []
	
	func update(page: Int, total_results: Int, total_pages: Int, results: NSArray) {
		//moviesCellViewModel.removeAll()
		self.page = page 
		self.total_results = total_results
		self.total_pages = total_pages
		
		for result in results {
			if let movieDict = result as? [String: Any] {
				let cellModel = MovieCellViewModel(resultDict: movieDict)
				moviesCellViewModel?.append(cellModel)
			}
		}
	}
}
