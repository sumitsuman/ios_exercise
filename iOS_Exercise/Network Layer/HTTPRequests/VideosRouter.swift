

import Alamofire

var movieId: Int = 0

enum VideosResultRouter: URLRequestConvertible {
	
	case getVideos(api_key: String, language: String)
	
	
	
	// MARK: - HTTPMethod
	private var method: HTTPMethod {
		switch self {
		case .getVideos:
			return .get
		}
	}
	
	// MARK: - Path
	private var path: String {
		switch self {
		case .getVideos:
			return "\(movieId)/videos"
		}
	}
	
	// MARK: - Parameters
	private var parameters: Parameters? {
		switch self {
		case .getVideos(let api_key, let language):
			return ["api_key": api_key, "language": language]
		}
	}
	
	var parameterEncoding: ParameterEncoding {
		switch self {
		case .getVideos:
			return URLEncoding.queryString
		}
	}
	
	
	// MARK: - URLRequestConvertible
	func asURLRequest() throws -> URLRequest {
        let url = try K.ProductionServer.baseURL.asURL()
        let encoding = parameterEncoding
        var urlRequest = try encoding.encode(URLRequest(url: url.appendingPathComponent(path)), with: parameters)

        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
		
		// Common Headers
		urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
		// Parameters
		if let parameters = parameters {
			do {
				urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions())
			} catch {
				throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
			}
		}
		debugPrint("============Request==============",String(describing:urlRequest), String(describing:urlRequest.httpHeaders))
		return urlRequest
	}
	
}

class VideosResultsRequest: NSObject {
	
	static func getVideoForMovieId(api_key: String, movieID: Int, language: String, completion:@escaping (Result<Any>)->Void) {
		movieId = movieID
        let _ = APIClient.performVideoRequest(movieId: movieId, completion: completion)
	}
}

