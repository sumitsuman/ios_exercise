import Foundation

extension KeyedDecodingContainer where Key: CodingKey {
    public func decodeDate(from key: Key, using formats: String...) throws -> Date {
        let dateAsString = try decode(String.self, forKey: key)
        let dateFormatter = DateFormatter()
        for format in formats {
            dateFormatter.dateFormat = format
            guard let date = dateFormatter.date(from: dateAsString) else { continue }
            return date
        }
        let context = DecodingError.Context(codingPath: [], debugDescription: "Cannot decode date: \(dateAsString)")
        throw DecodingError.dataCorrupted(context)
    }
}
