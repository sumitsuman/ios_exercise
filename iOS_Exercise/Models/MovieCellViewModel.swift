import Foundation

public class MovieCellViewModel {
  public var vote_count: Int
  public var id: Int
  public var video: Bool = false
  public var vote_average: Double
  public var title: String
  public var genre_ids: [Int] = []
  public var popularity: Double
  public var poster_path: String
  public var original_language: String
  public var original_title: String
  public var backdrop_path: String
  public var adult: Bool = false
  public var overview: String
  public var release_date: String
  
	public init(resultDict: [String: Any]) {
    self.vote_count = resultDict["vote_count"] as? Int ?? 0
    self.id = resultDict["id"] as? Int ?? 0
    self.video = resultDict["video"] as? Bool ?? false
    self.vote_average = resultDict["vote_average"] as? Double ?? 0.0
    self.title = resultDict["title"] as? String ?? ""
	self.genre_ids = resultDict["genre_ids"] as? [Int] ?? []
    self.popularity = resultDict["popularity"] as? Double ?? 0.0
    self.poster_path = resultDict["poster_path"] as? String ?? ""
    self.original_language = resultDict["original_language"] as? String ?? ""
    self.original_title = resultDict["original_title"] as? String ?? ""
    self.backdrop_path = resultDict["backdrop_path"] as? String ?? ""
    self.adult = resultDict["adult"] as? Bool ?? false
    self.overview = resultDict["overview"] as? String ?? ""
    self.release_date = resultDict["release_date"] as? String ?? ""
  }
}
