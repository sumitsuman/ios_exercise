import Foundation

extension Optional {
    public func matching(_ predicate: (Wrapped) -> Bool ) -> Wrapped? {
        guard let unwrappedValue = self, predicate(unwrappedValue) else {
            return .none
        }
        return unwrappedValue
    }

    public func or(_ otherExpression: @autoclosure () -> Wrapped) -> Wrapped {
        return self ?? otherExpression()
    }

    public func or(throw expresion: @autoclosure () -> Error) throws -> Wrapped {
        guard let unwrappedValue = self else {
            throw expresion()
        }
        return unwrappedValue
    }

    public func then(_ expression: (Wrapped) -> Void) {
        if case .some(let unwrappedValue) = self {
            expression(unwrappedValue)
        }
    }
}
