import UIKit

class SpinnerFooterView: UICollectionReusableView, NibNameableView, ReusableView {

    @IBOutlet private(set) weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private(set) weak var loadingMessageLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        activityIndicator.color = ColorPalette.grey
        loadingMessageLabel.textColor = ColorPalette.grey
        activityIndicator.startAnimating()
		loadingMessageLabel.text = "LOADING_MORE".localized(bundle: .main)
    }

    func start() {
        activityIndicator.isHidden = false
        loadingMessageLabel.isHidden = false
    }

    func stop() {
        activityIndicator.isHidden = true
        loadingMessageLabel.isHidden = true
    }

}
