import Foundation

public func base64Encoded(string: String) -> String? {
    return string.data(using: .utf8)?.base64EncodedString()
}

public func decodeBase64URL(_ url: String) -> Data? {
    var base64 = url
        .replacingOccurrences(of: "-", with: "+")
        .replacingOccurrences(of: "_", with: "/")

    let length = Double(base64.lengthOfBytes(using: .utf8))
    let requiredLength = 4 * ceil(length / 4.0)
    let paddingLength = requiredLength - length

    if paddingLength > 0 {
        let padding = "".padding(toLength: Int(paddingLength), withPad: "=", startingAt: 0)
        base64 += padding
    }

    return Data(base64Encoded: base64, options: .ignoreUnknownCharacters)
}

extension String {
    public static func isEmpty(_ string: String) -> Bool {
        return string.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }

    public static func isNotEmpty(_ string: String) -> Bool {
        return !isEmpty(string)
    }

    public var encoded: String? {
        return base64Encoded(string: self)
    }
}

extension String: LocalizedError {
    public var errorDescription: String? {
        return self
    }
}

// MARK: - Date Formats

extension String {
    public static let iso8601format = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    public static let iso8601fractionalFormat = "yyyy-MM-dd'T'HH:mm:ss.S'Z'"
    public static let monthDayYearAndTime = "MM/dd/yyy HH:mm:ss"
}

// MARK: - Localization

private func table(key: String, bundle: Bundle, tableName: String) -> (_ key: String, _ arguments: CVaListPointer) -> String {
    return { (key: String, params: CVaListPointer) in
        let content = key.localized(bundle: bundle, tableName: tableName)
        return NSString(format: content, arguments: params) as String
    }
}

extension String {
    public func localized(bundle: Bundle, tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, bundle: bundle, value: "", comment: "")
    }

    public func localized(arguments: CVarArg..., bundle: Bundle, tableName: String = "Localizable") -> String {
        let argumentsTable = table(key: self, bundle: bundle, tableName: tableName)
        return withVaList(arguments) { argumentsTable(self, $0) } as String
    }
}
