import Foundation

extension URL {
    public class Builder {
        private var components: URLComponents
        public init(scheme: String = "https", host: String) {
            components = URLComponents()
            components.scheme = scheme
            components.host = host
        }
    }
}

extension URL.Builder {
    public enum URLBuilderError: Swift.Error {
        case creatingURL
    }
}

extension URL.Builder {
    public func setScheme(_  scheme: String) -> URL.Builder {
        components.scheme = scheme
        return self
    }

    public func setHost(_  host: String) -> URL.Builder {
        components.host = host
        return self
    }

    public func setPort(_  port: Int) -> URL.Builder {
        components.port = port
        return self
    }

    public func setPath(_ path: String) -> URL.Builder {
        components.path = path
        return self
    }

    public func setQueryParam(key: String, value: Any) -> URL.Builder {
        if components.query == .none {
            components.queryItems = []
        }
        let queryItem = URLQueryItem(name: key, value: String(describing: value))
        components
            .queryItems?
            .append(queryItem)
        return self
    }

    public func build() throws -> URL {
        guard let url = components.url else {
            throw URLBuilderError.creatingURL
        }
        return url
    }
}
