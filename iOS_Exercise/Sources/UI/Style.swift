import UIKit

//---------------------------------
// MARK: - Styles
// We can add styles for our components here
//---------------------------------

enum Styles {
    enum Constant {
        static let cornerRadius: CGFloat = 3
        static let emailClearButtonInsets = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
        static let passwordVisibilityButtonInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

    // MARK: - Button styles
    static let roundedButton: Style<UIButton> = Style { button in
        button.clipsToBounds = true
        button.layer.cornerRadius = Constant.cornerRadius
    }

    static let primaryActionButton: Style<UIButton> = Style { button in
        button.titleLabel?.font = FontPalette.body
        button.titleLabel?.shadowOffset = .zero
        button.setTitleColor(ColorPalette.white, for: .normal)
        button.backgroundColor = ColorPalette.green
    }

    static let roundedPrimaryActionButton: Style<UIButton> = roundedButton.composing(with: primaryActionButton)

    static let roundedDefaultActionButton: Style<UIButton> = roundedPrimaryActionButton
        .composing(with: Style<UIButton> { button in
            button.backgroundColor = ColorPalette.blue
        })

    // MARK: - View styles
    static let roundedView: Style<UIView> = Style { view in
        view.layer.cornerRadius = Constant.cornerRadius
        view.clipsToBounds = true
    }

    static let primaryActionView: Style<UIView> = Style { view in
        view.backgroundColor = ColorPalette.green
    }

    static let roundedActionView: Style<UIView> = roundedView.composing(with: primaryActionView)

    static let roundedInactiveActionView: Style<UIView> = roundedActionView
        .composing(with: Style<UIView> { view in
            view.backgroundColor = ColorPalette.lightGrey
        })

    // MARK: - Switch  styles
    static let roundedSwitch: Style<UISwitch> = Style { switchButton in
        switchButton.layer.cornerRadius = switchButton.bounds.height/2
        switchButton.tintColor = ColorPalette.lightGrey
        switchButton.clipsToBounds = true
    }

    static let primaryActionSwitch: Style<UISwitch> = Style { switchButton in
        switchButton.onTintColor = ColorPalette.green
    }

    static let roundedSwitchAction: Style<UISwitch> = roundedSwitch.composing(with: primaryActionSwitch)

    static let SwitchOff: Style<UISwitch> = roundedSwitchAction
        .composing(with: Style<UISwitch> { switchButton in
            switchButton.backgroundColor = ColorPalette.lightGrey
        })

    // MARK: - UITextField  styles
    static let primaryTextField: Style<UITextField> = Style { textField in
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: textField.bounds.size.height))
        textField.leftViewMode = .always
        textField.layer.masksToBounds = true
    }

    static let topBorder: Style<UITextField> = Style { textField in
        let width = CGFloat(1.0)
        let topBorder = CALayer()
        topBorder.borderColor = ColorPalette.lightGrey.cgColor
        topBorder.frame = CGRect(x: 0, y: 0, width: textField.bounds.size.width, height: 1)
        topBorder.borderWidth = width
        textField.layer.addSublayer(topBorder)
    }

    static let bottomBorder: Style<UITextField> = Style { textField in
        let width = CGFloat(1.0)
        let bottomBorder = CALayer()
        bottomBorder.borderColor = ColorPalette.lightGrey.cgColor
        bottomBorder.frame = CGRect(x: 0, y: textField.bounds.size.height - 1, width: textField.bounds.size.width, height: 1)
        bottomBorder.borderWidth = width
        textField.layer.addSublayer(bottomBorder)
    }

    static let errorBottomBorder: Style<UITextField> = Style { textField in
        let width = CGFloat(1.0)
        let bottomBorder = CALayer()
        bottomBorder.borderColor = ColorPalette.red.cgColor
        bottomBorder.frame = CGRect(x: 0, y: textField.bounds.size.height - 1, width: textField.bounds.size.width, height: 1)
        bottomBorder.borderWidth = width
        textField.layer.addSublayer(bottomBorder)
    }

    static let arrowIcon: Style<UITextField> = Style { textField in
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 22, height: 22))
        let image = UIImage(named: "right-arrow")
        let width = imageView.bounds.size.width + 8
        let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 22))
        imageView.image = image
        view.addSubview(imageView)
        textField.rightViewMode = .always
        textField.rightView = view
    }

    static let topBorderTextField: Style<UITextField> = primaryTextField.composing(with: topBorder)

    static let bottomBorderTextField: Style<UITextField> = primaryTextField.composing(with: bottomBorder)

    static let arrowIconTextField: Style<UITextField> = primaryTextField.composing(with: arrowIcon)

    static let errorTextField: Style<UITextField> = bottomBorder.composing(with: errorBottomBorder)
}

public struct Style<View: UIView> {
    public let styling: (View) -> Void

    public init(_ styling: @escaping (View) -> Void) {
        self.styling = styling
    }
}

extension Style {
    public static func compose(_ styles: [Style]) -> Style<View> {
        return Style { view in
            styles.forEach { $0.styling(view) }
        }
    }

    public static func compose(_ styles: Style<View>...) -> Style<View> {
        return compose(styles)
    }

    public func composing(with other: Style<View>) -> Style<View> {
        return Style { view in
            self.styling(view)
            other.styling(view)
        }
    }

    public func composing(with otherStyling: @escaping (View) -> Void)-> Style<View> {
        return self.composing(with: Style(otherStyling))
    }
}

extension Style {
    public func apply(to view: View) {
        styling(view)
    }

    public func apply(to views: [View]) {
        views.forEach(styling)
    }

    public func apply(to views: View...) {
        apply(to: views)
    }
}

extension UIButton {
    public func apply(_ style: Style<UIButton>) {
        style.apply(to: self)
    }
}

extension UIView {
    public func apply(_ style: Style<UIView>) {
        style.apply(to: self)
    }
}

extension UISwitch {
    public func apply(_ style: Style<UISwitch>) {
        style.apply(to: self)
    }
}

extension UITextField {
    public func apply(_ style: Style<UITextField>) {
        style.apply(to: self)
    }
}
