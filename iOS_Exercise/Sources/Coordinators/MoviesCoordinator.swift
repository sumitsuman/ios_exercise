import UIKit

final class MoviesCoordinator {
    enum Constants {
        static let iconHeight: CGFloat = 22
        static let iconWidth: CGFloat = 22
    }
    let presenter: UINavigationController
    let customerId: String
    var childCoordinators: [Coordinator] = []
    
    init(presenter: UINavigationController, customerId: String) {
        self.presenter = presenter
        self.customerId = customerId
    }
}

// MARK: - Coordinator

extension MoviesCoordinator: Coordinator {
    func start() {
        presenter.isNavigationBarHidden = false
        presenter.navigationBar.prefersLargeTitles = true
        let popularMovieViewController = PopularMovieViewController.make(coordinator: self)
        popularMovieViewController.title = "POPULAR_MOVIES".localized(bundle: .main)
        presenter.setViewControllers([popularMovieViewController], animated: true)
    }
    
    func navigateToMovieDetails(with movie: MovieCellViewModel) {
        presenter.isNavigationBarHidden = false
        presenter.navigationBar.prefersLargeTitles = true
        let movieDetailsViewController = MovieDetailsViewController.make(coordinator: self, movie: movie)
        movieDetailsViewController.title = "MOVIE_DETAILS".localized(bundle: .main)
        presenter.navigationBar.tintColor = ColorPalette.white
        presenter.pushViewController(movieDetailsViewController, animated: true)
    }
}

