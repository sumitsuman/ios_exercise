import Foundation
import UIKit

public protocol NibNameableView: class {
    static var nibName: String { get }
}

extension NibNameableView where Self: UIView {
    static var nibName: String {
        return String(describing: self)
    }
}

extension NibNameableView where Self: UIViewController {
    static var nibName: String {
        return String(describing: self)
    }
}
