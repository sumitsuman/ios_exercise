
import UIKit

class NetworkResource {
    
    class public func serializeJsonFromData(data: Data?,usingKey jsonKey: String) -> (NSDictionary?, NSArray?) {
        if let jsonData = data {
            do {
                if let entityArray = try JSONSerialization.jsonObject(with: jsonData as Data, options: JSONSerialization.ReadingOptions()) as? NSArray {
                    return (nil, entityArray)
                }
                
                let jsonObject: NSDictionary? = try JSONSerialization.jsonObject(with: jsonData as Data, options: JSONSerialization.ReadingOptions()) as? NSDictionary
                if let dataDictionary = jsonObject?.object(forKey: "data") {
                    
                    
                    if let entityDictionary = (dataDictionary as AnyObject).object(forKey: jsonKey) as? NSDictionary {
                        return (entityDictionary, nil)
                    } else {
                        if let entityArray = (dataDictionary as AnyObject).object(forKey: jsonKey) as? NSArray {
                            return (nil, entityArray)
                        }
                    }
                }
                
                if let dataDictionary = jsonObject?.object(forKey: jsonKey)  {
                    
                    if let entityDictionary = dataDictionary as? NSDictionary {
                        return (entityDictionary, nil)
                    } else {
                        if let entityArray = (dataDictionary as? NSArray){
                            return (nil, entityArray)
                        }
                    }
                } else{
                    
                    return (jsonObject, nil)
                }
                
                return (nil, nil)
            } catch  _ {
                return (nil, nil)
            }
        }
        return (nil, nil)
    }

}


