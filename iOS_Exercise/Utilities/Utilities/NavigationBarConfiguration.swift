import Foundation
import UIKit

 enum NavigationBarConfiguration {
    static func setupAppearance() {
        UINavigationBar.appearance().largeTitleTextAttributes = [.foregroundColor: ColorPalette.white ]
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: ColorPalette.white ]
        UINavigationBar.appearance().barTintColor = ColorPalette.black
        UINavigationBar.appearance().tintColor = ColorPalette.black
        UINavigationBar.appearance().isTranslucent =  false
        UINavigationBar.appearance().backgroundColor = ColorPalette.black
    }
}
