import Foundation

public protocol Localizable {
    var key: String { get }
    func localized(bundle: Bundle, tableName: String) -> String
    static var tableName: String { get }
}

extension Localizable {
    public static var tableName: String {
        return String(describing: self)
    }

    public func localized(bundle: Bundle = .main, tableName: String = Self.tableName) -> String {
        return key.localized(bundle: bundle, tableName: tableName)
    }
}

extension Localizable where Self: RawRepresentable, Self.RawValue == String {
    public var key: String {
        return rawValue
    }
}
