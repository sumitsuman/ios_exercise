import UIKit

class MovieDetailsViewController: UIViewController, NibNameableView {
    
    @IBOutlet weak var tableView: UITableView!
    private weak var coordinator: MoviesCoordinator?
    private var movie: MovieCellViewModel?
    
    lazy var videos: [TrailerViewModel]? = []
    
    enum TableViewSection: Int, CaseIterable {
        case title
        case poster
        case synopsis
        case trialer
    }
    
    static func make(coordinator: MoviesCoordinator, movie: MovieCellViewModel) -> MovieDetailsViewController {
        return MovieDetailsViewController(coordinator: coordinator, movie: movie)
    }
    
    private init(coordinator: MoviesCoordinator, movie: MovieCellViewModel) {
        self.coordinator = coordinator
        self.movie = movie
        super.init(nibName: MovieDetailsViewController.nibName, bundle: .main)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(cell: TitleTableViewCell.self, inBundle: .main)
        tableView.register(cell: PosterTableViewCell.self, inBundle: .main)
        tableView.register(cell: SynopsisTableViewCell.self, inBundle: .main)
        tableView.register(cell: TrailerTableViewCell.self, inBundle: .main)
        tableView.dataSource = self
        guard let movieId = movie?.id else {
            return
        }
        getVideos(with: movieId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

extension MovieDetailsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = TableViewSection(rawValue: section) else {
            return 0
        }
        switch section {
        case .trialer:
            return videos?.count ?? 0
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = TableViewSection(rawValue: indexPath.section) else {
            return UITableViewCell()
        }
        switch section {
        case .title: let cell: TitleTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.titleLabel.text = movie?.title
        return cell
        case .poster: let cell: PosterTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configure(with: movie)
        return cell
        case .synopsis: let cell: SynopsisTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configure(with: movie)
        return cell
        case .trialer: let cell: TrailerTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        if let viewModel = videos?[indexPath.row]  {
            cell.configure(with: viewModel)
        }
        return cell
        }
    }
}

extension MovieDetailsViewController {
    func getVideos(with movieID: Int) {
        VideosResultsRequest.getVideoForMovieId(api_key:"b1b4bf87e5648c80ba62a374ee4c16e4" , movieID: movieID, language: "en-US"){ (result) in
            
            switch result {
            case .success(let dropDownData):
                if let JSONData = dropDownData as? [String: Any], let resultArray = JSONData["results"] as? NSArray, resultArray.count > 0 {
                    print(JSONData)
                    for result in resultArray {
                        if let videoListDict = result as? [String: Any] {
                            let cellModel = TrailerViewModel(resultDict: videoListDict)
                            self.videos?.append(cellModel)
                        }
                    }
                    DispatchQueue.main.async{
                        self.tableView.reloadData()
                    }
                }
                
            case .failure(let error):
                print("error", error.localizedDescription)
            }
        }
    }	
}
