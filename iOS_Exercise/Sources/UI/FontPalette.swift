import UIKit

enum FontPalette {
    // Regular 28pt
    static let title = UIFont.preferredFont(forTextStyle: .title1)

    // Regular 22pt
    static let subtitle = UIFont.preferredFont(forTextStyle: .title2)

    // Regular 20pt
    static let subsubTitle = UIFont.preferredFont(forTextStyle: .title3)

    // Semi Bold 17pt
    static let headline = UIFont.preferredFont(forTextStyle: .headline)

    // Regular 17 pt
    static let body = UIFont.preferredFont(forTextStyle: .body)

    // Regular 15pt
    static let bodyInformation = UIFont.preferredFont(forTextStyle: .subheadline)

    // Regular 13pt
    static let informational = UIFont.preferredFont(forTextStyle: .footnote)
}
