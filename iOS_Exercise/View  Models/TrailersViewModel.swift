import Foundation

public class TrailerViewModel {
	public var id: String?
	public var key: String?
	public var name: String?
	public var site: String?
	public var size: Int?
	public var type: String?
	
	public init(resultDict:[String: Any]) {
		self.id = resultDict["id"] as? String
		self.key = resultDict["key"] as? String
		self.name = resultDict["name"] as? String
		self.site = resultDict["site"] as? String
		self.size = resultDict["size"] as? Int
		self.type = resultDict["type"] as? String
	}
}

