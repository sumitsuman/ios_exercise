import UIKit
import Nuke

class MovieCollectionViewCell: UICollectionViewCell, NibNameableView, ReusableView {
	
	@IBOutlet weak var imageView: UIImageView!
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}
	
	func configure(with viewModel: MovieCellViewModel?) {
		if let posterPath = viewModel?.poster_path, let url = URL(string: "https://image.tmdb.org/t/p/w185/\(posterPath)") {
			Nuke.loadImage(with: url, into: imageView)
		}
	}
}
