import Foundation
import UIKit

extension CGSize {
    func widthScaling(with value: CGFloat) -> CGSize {
        return CGSize(width: value, height: (value * height) / width)
    }
}
