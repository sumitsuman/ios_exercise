import UIKit

extension UIViewController {
    func displayError(message: String, withTitle title: String?, onOkHandler: (() -> Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in
            onOkHandler?()
        }
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: .none)
    }
}
