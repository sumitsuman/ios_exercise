import UIKit
import Nuke

class PosterTableViewCell: UITableViewCell, NibNameableView, ReusableView {
	
	@IBOutlet weak var posterImageView: UIImageView!
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

	func configure(with viewModel: MovieCellViewModel?) {
		if let posterPath = viewModel?.poster_path, let url = URL(string: "https://image.tmdb.org/t/p/w500/\(posterPath)") {
			Nuke.loadImage(with: url, into: posterImageView)
		}
	}
    
}
