

import Foundation


struct K {
    struct ProductionServer {
        static let baseURL = "https://api.themoviedb.org/3/movie/"
    }
    
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
    case cacheControl = "cache-control"
    case postmanToken = "Postman-Token"
    case userAgent = "User-Agent"
    case cookie = "cookie"
    case contentLength = "content-length"
    case connection = "Connection"
    
}

enum ContentType: String {
    case json = "application/json"
    case xml = "application/xml"
    case urlEncode = "application/x-www-form-urlencoded"
}

enum cacheControl: String {
    case cacheControl = "no-cache"
}

enum PostmanToken: String {
    case postmanToken = "90df77e8-45c6-4fd1-809e-7a3a45c51f85"
}

enum Cookie: String {
    case cookie = "ARRAffinity=0b3e7bab7bc253429b01a84fcfe52f9eaf7487da416b1f2a0af2bf3349b35509"
}

enum AcceptEncoding: String {
    case acceptEncoding = "gzip, deflate"
}
enum Accept: String {
    case accept = "/"
}

enum ContentLength: String {
    case contentLength = "81"
}
enum Connection: String {
    case connection = "keep-alive"
}


