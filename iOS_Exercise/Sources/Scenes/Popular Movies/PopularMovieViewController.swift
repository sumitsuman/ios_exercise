import UIKit

class PopularMovieViewController: UIViewController, NibNameableView {
    
    private weak var coordinator: MoviesCoordinator?
    
    static func make(coordinator: MoviesCoordinator) -> PopularMovieViewController {
        return PopularMovieViewController(coordinator: coordinator)
    }
    
    private init(coordinator: MoviesCoordinator) {
        self.coordinator = coordinator
        super.init(nibName: PopularMovieViewController.nibName, bundle: .main)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let viewModel: PopularMoviesViewMOdel = PopularMoviesViewMOdel()
    private var rootViewModel: RootViewModel?
    private var pageNumber = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        rootViewModel = RootViewModel()
        collectionView.register(cell: MovieCollectionViewCell.self, in: .main)
        getMovieData()
        collectionView.register(view: SpinnerFooterView.self, in: .main, asSupplementaryViewKind: .footer)
    }
}

extension PopularMovieViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return rootViewModel?.moviesCellViewModel?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MovieCollectionViewCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configure(with: rootViewModel?.moviesCellViewModel?[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard kind == UICollectionView.SupplementaryViewKind.footer.rawValue else {
            return UICollectionReusableView()
        }
        return collectionView.dequeueReusableSupplementaryView(ofKind: .footer, forIndexPath: indexPath) as SpinnerFooterView
    }
}

// MARK: - UICollectionViewDelegate

extension PopularMovieViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let movieModel = rootViewModel?.moviesCellViewModel?[indexPath.row] {
            coordinator?.navigateToMovieDetails(with: movieModel)
        }
    }
}

extension PopularMovieViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return viewModel.insetsInSection
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return viewModel.itemsSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return viewModel.itemsSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return viewModel.sizeForItem(fromWidth: collectionView.frame.width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return .zero
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let dataCount = rootViewModel?.moviesCellViewModel?.count, (dataCount - 1) == indexPath.row {
            pageNumber += 1
            getMovieData()
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        guard elementKind == UICollectionView.SupplementaryViewKind.footer.rawValue, let footer = view as? SpinnerFooterView else {
            return
        }
        footer.stop()
    }
}

extension PopularMovieViewController {
    func getMovieData() {
        MoviesResultsRequest.getMoviesResults(api_key: "b1b4bf87e5648c80ba62a374ee4c16e4", lang: "en-US", pageNo: pageNumber) { (result) in
            
            switch result {
            case .success(let dropDownData):
                if let JSONData = dropDownData as? [String: Any], let resultArray = JSONData["results"] as? NSArray, resultArray.count > 0 {
                    print(JSONData)
                    self.rootViewModel?.update(page: JSONData["page"] as? Int ?? 0, total_results: JSONData["total_results"] as? Int ?? 0, total_pages: JSONData["total_pages"] as? Int ?? 0, results: resultArray )
                    self.collectionView.reloadData()
                }
                
            case .failure(let error):
                print("error", error.localizedDescription)
            }
        }
    }
}



