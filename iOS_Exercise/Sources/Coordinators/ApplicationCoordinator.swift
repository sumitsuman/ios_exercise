import UIKit

final class ApplicationCoordinator {
    
    let window: UIWindow
    var childCoordinators: [Coordinator] = []
    
    private lazy var navigationController: UINavigationController = {
        let controller = UINavigationController()
        controller.view.backgroundColor = ColorPalette.white
        return controller
    }()
    
    init(window: UIWindow) {
        self.window = window
        self.window.rootViewController = navigationController
        self.window.makeKeyAndVisible()
    }
    
}

// MARK: - Coordinator

extension ApplicationCoordinator: Coordinator {
    func start() {
        presentMoviesListFlow(with: "customerId")
    }
}

// MARK: - Flows

extension ApplicationCoordinator {
    func presentMoviesListFlow(with customerId: String) {
        let moviesCoordinator = MoviesCoordinator(presenter: navigationController, customerId: customerId)
        moviesCoordinator.start()
        addChild(coordinator: moviesCoordinator)
    }
}

// MARK: - Default Alert presentation

extension ApplicationCoordinator {
    private func presentAlert(title: String, message: String, onOK action: (() -> Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (_) in
            action?()
        }
        alertController.addAction(okAction)
        navigationController.present(alertController, animated: true, completion: .none)
    }
}
